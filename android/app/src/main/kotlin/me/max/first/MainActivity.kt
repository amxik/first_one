package me.max.first

import android.os.Bundle
import com.microsoft.appcenter.AppCenter
import com.microsoft.appcenter.distribute.Distribute
import io.flutter.app.FlutterActivity
import io.flutter.plugins.GeneratedPluginRegistrant
import me.max.first.BuildConfig.APP_CENTER_KEY


class MainActivity: FlutterActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    GeneratedPluginRegistrant.registerWith(this)
    AppCenter.start(application, APP_CENTER_KEY, Distribute::class.java)

  }
}
