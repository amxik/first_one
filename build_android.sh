#!/usr/bin/env bash

set -e

FLAVOR=${CI_ENVIRONMENT_NAME}

BUILD_NUMBER=${CI_PIPELINE_IID}
BUILD_NAME="1.0.${BUILD_NUMBER}"

if [[ "${FLAVOR}" = "production" ]]; then
    flutter build appbundle --flavor ${FLAVOR} --build-name=${BUILD_NAME} --build-number=${BUILD_NUMBER}
else
    flutter build apk --flavor ${FLAVOR} --build-name=${BUILD_NAME} --build-number=${BUILD_NUMBER}
fi
