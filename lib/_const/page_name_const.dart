class PageName {
  static const EXPANDED = "Expanded";
  static const FLOW = "Flow";
  static const ROW_COLUMN = "Row & Column";
  static const STACK = "Stack";
  static const WRAP = "Wrap";
}
