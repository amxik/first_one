import 'package:first/my_widgets/MyExpanded.dart';
import 'package:first/my_widgets/MyFlow.dart';
import 'package:first/my_widgets/MyRowAndColumn.dart';
import 'package:first/my_widgets/MyStack.dart';
import 'package:first/my_widgets/MyWrap.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  bool isEnabled = true;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  void _decrementCounter() {
    setState(() {
      _counter--;
    });
  }

  @override
  Widget build(BuildContext context) {
    RaisedButton myButton = RaisedButton(
      child: Text("Hello!)"),
      disabledColor: Colors.brown,
      disabledTextColor: Colors.white,
      textColor: Colors.green,
      highlightColor: Colors.deepOrange,
      hoverColor: Colors.deepPurpleAccent,
      onPressed: isEnabled ? _decrementCounter : null,
    );


    return Scaffold(
      appBar: AppBar(

        title: Text(widget.title),
      ),
      body: Center(

        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,

              children: <Widget>[
                RaisedButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => RowColumnPage()
                        ));
                  },
                  child: Text('Row & Column', style: TextStyle(fontSize: 20),),
                ), RaisedButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => MyWrapWidget()
                        ));
                  },
                  child: Text('Wrap', style: TextStyle(fontSize: 20),),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,

              children: <Widget>[
                RaisedButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => MyFlowExample()
                        ));
                  },
                  child: Text('Flow', style: TextStyle(fontSize: 20),),
                ), RaisedButton(
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(
                        builder: (context) => MyExpandedWidget()));
                  },
                  child: Text('Expanded', style: TextStyle(fontSize: 20),),
                ), RaisedButton(
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(
                        builder: (context) => MyStackWidget()
                    ));
                  },
                  child: Text('Stack', style: TextStyle(fontSize: 20),),
                ),
              ],
            ),
            Icon(
              Icons.ac_unit,
              semanticLabel: "Снежинка)",
            ),
            Text(
              'You have pushed the button this many times:',
              style: TextStyle(letterSpacing: 2),
              softWrap: true,
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.display1,
            ),
            Text(myButton.enabled ? 'Кнопка активна' : 'Кнопка неактивна'),
            ButtonTheme(
              buttonColor: Colors.amberAccent,
              padding: EdgeInsets.only(bottom: 50),
              height: 50,
              minWidth: 100,
              child: myButton,),
            RaisedButton(
              onPressed: () {
                setState(() {
                  isEnabled = !isEnabled;
                });
              },
            )

          ],
        ),
      ),

      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}
