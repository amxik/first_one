#!/usr/bin/env bash
set -e

cd android

bundle install

bundle exec fastlane uploadappcenter
