#!/usr/bin/env bash

set -e

flutter format lib
flutter analyze
flutter test --coverage
